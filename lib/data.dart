// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

library eqnb.data;

import 'dart:async';

import 'package:eqlib/utils.dart';
import 'package:eqlib/eqlib.dart';
import 'package:eqlib/latex_printer.dart';
import 'package:angular2/angular2.dart';

part 'src/data/eq_info.dart';
part 'src/data/apply_def.dart';
part 'src/data/symbol_data.dart';
part 'src/data/expr_engine.dart';
part 'src/data/entry_data.dart';

/// All possible entry types
enum EntryType { empty, symbols, define, apply, eval }

/// Function to get an [EntryData] record by index.
typedef EntryData EntryLookupFn(int i);
