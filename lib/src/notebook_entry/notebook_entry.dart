// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

import 'dart:html';
import 'dart:async';

import 'package:eqnb/data.dart';

import 'package:angular2/angular2.dart';
import 'package:angular2_components/angular2_components.dart';

import '../latex/latex.dart';
import '../symbol_table/symbol_table.dart';
import '../equation_editor/equation_editor.dart';

/// Use this instead of CSS transitions because we need all kinds of shady
/// delays to make them work.
void _animateHeight(
    DivElement elm, int from, int to, double stepSize, Function callback,
    [int steps = 0]) {
  window.requestAnimationFrame((num time) {
    elm.style.height = '${(from + stepSize * steps).round()}px';
    if (steps < ((to - from) / stepSize).abs().floor()) {
      _animateHeight(elm, from, to, stepSize, callback, steps + 1);
    } else {
      elm.style.height = '${to}px';
      callback();
    }
  });
}

@Component(
  selector: 'notebook-entry',
  templateUrl: 'notebook_entry.html',
  styleUrls: const ['notebook_entry.css'],
  directives: const [
    materialDirectives,
    SymbolTableComponent,
    EquationEditorComponent,
    LatexComponent
  ],
  providers: const [materialProviders],
)
class NotebookEntryComponent implements AfterContentInit {
  static const animSteps = 10;

  @Input()
  EntryData data;

  @Output()
  Stream<EntryType> get typeChanged => _typeChanged.stream;

  @Output()
  Stream<Null> get entryInsert => _entryInsert.stream;

  @Output()
  Stream<Null> get entryDelete => _entryDelete.stream;

  String applyText;

  // Stream controllers for bindings with the parent notebook.
  final _typeChanged = new StreamController<EntryType>();
  final _entryInsert = new StreamController<Null>();
  final _entryDelete = new StreamController<Null>();

  @ViewChild('wrapper')
  ElementRef wrapper;

  void ngAfterContentInit() {
    final div = wrapper.nativeElement as DivElement;
    div.style.height = 'auto';

    if (data.type == EntryType.empty) {
      // Animate wrapper when entry is new.
      _animateHeight(div, 0, div.clientHeight, div.clientHeight / animSteps,
          () {
        div.style.height = 'auto';
      });
      div.style.height = '0';
    }

    // Load applyText from data.lastApply once.
    if (data.lastApply != null) {
      applyText = data.lastApply.toString();
    }
  }

  void configure(int typeIndex) {
    data.type = EntryType.values[typeIndex];
    _typeChanged.add(data.type);

    if (data.type == EntryType.symbols) {
      // Add initial row.
      data.symbols.add(new SymbolData('', ''));
    } else if (data.type == EntryType.eval) {
      data.setupEval();
    }
  }

  void updateApply() {
    // Compute new apply definition.
    data.applyInput.add(new ApplyDef.fromString(applyText));
  }

  void onDelete() {
    final div = wrapper.nativeElement as DivElement;
    _animateHeight(div, div.clientHeight, 0, -div.clientHeight / animSteps, () {
      _entryDelete.add(null);
    });
  }

  void onInsert() => _entryInsert.add(null);
}
