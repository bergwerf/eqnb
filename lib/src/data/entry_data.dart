// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

part of eqnb.data;

class EntryData {
  /// LaTeX printer
  final LaTeXPrinter printer;

  /// Entry index in the notebook
  int index;

  /// Entry type
  EntryType type = EntryType.empty;

  /// For [EntryType.symbols] entries
  var symbols = new List<SymbolData>();

  /// For [EntryType.define] entries (only used for data transfer)
  var definedEquation = new W<String>('');

  /// Equation pipeline
  final eqInput = new StreamController<EqInfo>.broadcast();
  EqInfo lastEq;

  /// Apply defenition pipeline
  final applyInput = new StreamController<ApplyDef>();
  ApplyDef lastApply;

  /// LaTeX Stream for equation rendering.
  String currentExpression = '';
  StreamSubscription _oldDictSub;

  final EntryLookupFn entryLookup;

  EntryData(this.printer, this.index, this.entryLookup) {
    initialize();
  }

  /// Reconstruct from JSON.decode data.
  EntryData.fromJson(
      this.printer, this.entryLookup, Map<String, dynamic> data) {
    initialize();
    index = data['index'];
    type = EntryType.values[data['type']];

    // Parse data parameter based on type.
    if (type == EntryType.symbols) {
      List symbolData = data['data'];
      if (symbolData != null) {
        symbols = new List<SymbolData>.generate(
            symbolData.length,
            (i) => new SymbolData.fromJson(
                new Map<String, String>.from(symbolData[i])));
      }
    } else if (type == EntryType.define) {
      definedEquation = new W<String>(data['data'] ?? '');
      eqInput.add(evaluateEquation(definedEquation.v));
    } else if (type == EntryType.apply) {
      applyInput.add(new ApplyDef.fromJson(data['data']));
    } else if (type == EntryType.eval) {
      setupEval();
    }
  }

  void initialize() {
    onApply.listen((apply) {
      // Terminate previous pipeline.
      if (lastApply != null) {
        lastApply.destroyPipeline();
      }
      lastApply = apply;

      // Create new equation pipeline.
      apply.createPipeline(index, eqInput, entryLookup);
    });

    // Generate LaTeX new LaTeX expressions in response to new expressions.
    onEq.listen((equation) {
      lastEq = equation;

      // Cancel old LaTeX dictionary subscription.
      if (_oldDictSub != null) {
        _oldDictSub.cancel();
      }

      // Stream formatted expression to [_expression], and keep a dictonary
      // subscription so we can terminate this stream in the next cycle.
      _oldDictSub = printer.onDictUpdate.listen((_) {
        // Push updated expression.
        currentExpression = equation.generateLaTeX(printer);
      });
      currentExpression = equation.generateLaTeX(printer);
    });
  }

  Stream<EqInfo> get onEq => eqInput.stream;
  Stream<ApplyDef> get onApply => applyInput.stream;

  void setupEval() {
    if (index == 0) {
      return;
    }

    // Setup pipeline.
    final source = entryLookup(index - 1);
    source.onEq.listen(evalRefresh);
    if (source.lastEq != null) {
      evalRefresh(source.lastEq);
    }
  }

  void evalRefresh(EqInfo input) {
    input = input.clone();
    input.eq.eval();
    eqInput.add(input);
  }

  /// Add [change] to all indices starting at [start] (inclusive).
  void changeIndicesFrom(int start, int change) {
    if (index >= start) {
      index += change;
    }

    // There is not really an implication for the equation pipeline. Only the
    // text in the apply input needs to be updated.
    if (lastApply != null) {
      lastApply.changeIndicesFrom(start, change);
    }
  }

  Map<String, dynamic> toJson() {
    switch (type) {
      case EntryType.empty:
      case EntryType.eval:
        return {'index': index, 'type': type.index};
      case EntryType.symbols:
        return {'index': index, 'type': type.index, 'data': symbols};
      case EntryType.define:
        return {'index': index, 'type': type.index, 'data': definedEquation.v};
      case EntryType.apply:
        return {'index': index, 'type': type.index, 'data': lastApply};
      default:
        throw new StateError('EntryType.data is $type');
    }
  }
}
