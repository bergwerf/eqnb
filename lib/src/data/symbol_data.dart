// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

part of eqnb.data;

/// Symbol information
class SymbolData {
  /// Symbol name
  String name;

  /// Symbol LaTeX expression
  String latex;

  SymbolData(this.name, this.latex);

  SymbolData.fromJson(Map<String, String> data) {
    name = data['name'];
    latex = data['latex'];
  }

  Map<String, String> toJson() => {'name': name, 'latex': latex};
}
