// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

part of eqnb.data;

/// Equation information
/// TODO: filter out generics that are invisible in the equation.
class EqInfo {
  final Eq eq;
  final Set<int> generic;
  final bool wrap;
  EqInfo(this.eq, this.generic, this.wrap);

  EqInfo clone() => new EqInfo(eq.clone(), new Set<int>.from(generic), wrap);

  /// Generate LaTeX string that represents this equation.
  String generateLaTeX(LaTeXPrinter printer) {
    final g = generic.toList();
    final gs = new List<String>.generate(
        g.length, (i) => dfltExprEngine.resolveName(g[i]));
    final genericLaTeX = generic.isEmpty
        ? ''
        : '\\quad\\left(\\text{generic: ${gs.join(', ')}}\\right)';

    // Render left and right side of the equation.
    final left = printer.render(eq.left, dfltExprEngine.resolveName);
    final right = printer.render(eq.right, dfltExprEngine.resolveName);

    // Render final equation.
    if (wrap) {
      return '$left{\\ \\longrightarrow\\ }$right$genericLaTeX';
    } else {
      return '$left=$right$genericLaTeX';
    }
  }

  String toString() => eq.toString();
}

/// Specify a wrapping relation.
const thenStr = '->';

/// Evaluate equation information string.
EqInfo evaluateEquation(String text,
    [ExprResolve resolve = standaloneResolve]) {
  // Terminate right away if the text is empty.
  if (text == null || text.isEmpty) {
    return null;
  }

  try {
    // Parse generics.
    var generic = new List<int>();
    var eqText = text;
    if (text.contains('?')) {
      final parts = text.split('?');
      eqText = parts.first;
      final strs = parts.last.split(',')..forEach((value) => value.trim());
      generic = new List<int>.generate(strs.length, (i) => resolve(strs[i]));
    }

    // Split left and right side.
    final wrap = eqText.contains(thenStr);
    final sides = wrap ? eqText.split(thenStr) : eqText.split('=');

    // Parse expressions.
    final p = new EqExParser();
    final eq = new Eq(p.parse(sides.first).value, p.parse(sides.last).value);
    return new EqInfo(eq, generic.toSet(), wrap);
  } catch (e) {
    return null;
  }
}
