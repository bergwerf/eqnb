// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

part of eqnb.data;

/// Data of one apply cycle.
class ApplyStep {
  int target; // Not final because it can be displaced by [changeIndicesFrom]
  final int index;
  final bool recursive;

  // Pipeline data
  StreamController<EqInfo> _outputStream;
  StreamSubscription _sourceSub, _targetSub;

  ApplyStep(this.target, [this.index = 0, this.recursive = false]);

  /// String format: `12:recursive#3` (target: 12, recursive: true, index: 3)
  factory ApplyStep.fromString(String str) {
    final params = str.trim().split(new RegExp('[:#]'));
    final target = int.parse(params.first);
    params.removeAt(0);
    final recursive = params.remove('recursive');

    // Parse index.
    var index = 0;
    try {
      index = int.parse(params.first);
    } catch (e) {}

    return new ApplyStep(target, index, recursive);
  }

  /// Add [change] to all indices starting at [start] (inclusive).
  void changeIndicesFrom(int start, int change) {
    if (target != null && target >= start) {
      target += change;
    }
  }

  Stream<EqInfo> createPipeline(Stream<EqInfo> source, EntryLookupFn lookup) {
    assert(_outputStream == null && _sourceSub == null && _targetSub == null);

    // Get target stream.
    final targetEntry = lookup(target);

    // Create output stream controller.
    _outputStream = new StreamController<EqInfo>();

    EqInfo sourceCache, targetCache = targetEntry.lastEq;

    // Trigger refresh when either the source or the target stream is updated.
    _sourceSub = source.listen((eq) {
      sourceCache = eq;
      refresh(eq, targetCache);
    });
    _targetSub = targetEntry.onEq.listen((eq) {
      targetCache = eq;
      refresh(sourceCache, eq);
    });

    return _outputStream.stream;
  }

  void destroyPipeline() {
    assert(_sourceSub != null && _targetSub != null && _outputStream != null);
    _sourceSub.cancel();
    _sourceSub = null;
    _targetSub.cancel();
    _targetSub = null;
    _outputStream.close();
    _outputStream = null;
  }

  void refresh(EqInfo source, EqInfo target) {
    // If one of the parameters is null, terminate.
    if (source == null || target == null) {
      return;
    }

    // TODO: eliminate non-required clones.
    source = source.clone();

    // Merge with storedEq generic.
    source.generic.addAll(target.generic);
    final generic = source.generic.toList(growable: false);

    // If this is a wrap relation, use a different routine.
    if (target.wrap) {
      source.eq.wrap(target.eq.left, generic, target.eq.right);
    } else {
      source.eq.subs(target.eq, generic, index);
    }

    // Add source to output stream.
    _outputStream.add(source);
  }

  String toString() =>
      '$target' +
      (recursive ? ':recursive' : '') +
      (index > 0 ? '#$index' : '');
}

/// Full definition of apply rules.
class ApplyDef {
  int source = -1;
  List<ApplyStep> steps = [];

  // Pipeline data
  StreamController<EqInfo> _inputStream;
  StreamSubscription _inputSub, _outputSub;

  ApplyDef.empty();

  ApplyDef.fromString(String data) {
    parse(data);
  }

  ApplyDef.fromJson(String data) {
    parse(data);
  }

  String toJson() => toString();

  void createPipeline(
      int contextIndex, StreamController<EqInfo> output, EntryLookupFn lookup) {
    assert(_inputSub == null && _outputSub == null && _inputStream == null);

    // Source is either the previous index or the first apply parameter.
    final sourceEntry = lookup(source == -1 ? contextIndex - 1 : source);
    _inputStream = new StreamController<EqInfo>();
    Stream<EqInfo> prevStream = _inputStream.stream;

    // Collect streams from all apply steps.
    for (final apply in steps) {
      prevStream = apply.createPipeline(prevStream, lookup);
    }

    // Add final output to output.
    _outputSub = prevStream.listen((data) => output.add(data));

    // Trigger one processing pass.
    if (sourceEntry.lastEq != null) {
      _inputStream.add(sourceEntry.lastEq);
    }
    _inputSub = sourceEntry.onEq.listen((data) {
      _inputStream.add(data);
    });
  }

  void destroyPipeline() {
    assert(_inputSub != null && _outputSub != null && _inputStream != null);
    _inputSub.cancel();
    _inputSub = null;
    _outputSub.cancel();
    _outputSub = null;
    _inputStream.close();
    _inputStream = null;
    for (final apply in steps) {
      apply.destroyPipeline();
    }
  }

  /// Add [change] to all indices starting at [start] (inclusive).
  void changeIndicesFrom(int start, int change) {
    if (source >= start) {
      source += change;
    }
    steps.forEach((elm) => elm.changeIndicesFrom(start, change));
  }

  /// Parse applyText.
  void parse(String text) {
    if (text == null || text.isEmpty) {
      return;
    }

    // Parse source index.
    if (text.contains(';')) {
      final parts = text.split(';');
      source = int.parse(parts.first.trim());
      text = parts.last;
    }

    // Parse apply steps.
    final items = text.split(',');
    for (final item in items) {
      steps.add(new ApplyStep.fromString(item));
    }
  }

  /// Generate string.
  String toString() {
    if (source == -1) {
      return steps.join(', ');
    } else {
      return '$source; ${steps.join(', ')}';
    }
  }
}
