// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

part of eqnb.data;

@Injectable()
class ExprEngine {
  final printer = new LaTeXPrinter();
  ExprEngine() {
    printer.addDefaultEntries(dfltExprEngine.resolve);
  }
}
