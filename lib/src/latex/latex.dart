// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

import 'package:katex_js/katex_js.dart' as katex;

import 'package:angular2/angular2.dart';

@Component(
  selector: 'latex',
  template: '<span #target></span>',
)
class LatexComponent {
  @ViewChild('target')
  ElementRef target;

  @Input()
  set expression(String value) {
    if (value != null) {
      katex.render(value, target.nativeElement);
    }
  }
}
