// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

import 'package:eqnb/data.dart';
import 'package:eqlib/eqlib.dart';
import 'package:katex_js/katex_js.dart' as katex;

import 'package:angular2/angular2.dart';
import 'package:angular2_components/angular2_components.dart';

@Component(
  selector: 'symbol-table',
  templateUrl: 'symbol_table.html',
  styleUrls: const ['symbol_table.css'],
  directives: const [materialDirectives, MaterialNumberInputValidatorDirective],
  providers: const [materialProviders],
)
class SymbolTableComponent implements AfterViewInit {
  /// Keep track of old names to safely delete them from printers etc.
  final oldNames = new List<String>();

  final ExprEngine engine;

  @Input()
  List<SymbolData> symbols;

  @ViewChildren('renderedSymbol')
  QueryList<ElementRef> renderedSymbol;

  SymbolTableComponent(this.engine);

  void ngAfterViewInit() {
    // Render all LaTeX strings.
    for (var i = 0; i < symbols.length; i++) {
      oldNames.add('');
      updateSymbol(i);
    }
  }

  void addSymbol(int index) {
    symbols.insert(index + 1, new SymbolData('', ''));
    oldNames.insert(index + 1, '');
  }

  void updateSymbol(int index) {
    final name = symbols[index].name;
    final latex = symbols[index].latex;

    // Update data.
    assert(oldNames.length > index);
    final newName = dfltExprEngine.resolve(name);
    final oldName = oldNames[index] != ''
        ? dfltExprEngine.resolve(oldNames[index])
        : newName;
    engine.printer.dictReplace(oldName, newName, latex);

    // Render LaTeX string.
    try {
      // Replace all interpolation expressions.
      final stripped =
          latex.replaceAllMapped(new RegExp(r'\$\(*(\w+)\)*'), (match) {
        return '\\text{${match.group(1)}}';
      });
      katex.render(stripped, renderedSymbol.toList()[index].nativeElement);
    } catch (e) {}
  }

  void removeSymbol(int index) {
    // TODO: also safely remove symbols when the entire table is deleted.
    symbols.removeAt(index);
    oldNames.removeAt(index);
  }
}
