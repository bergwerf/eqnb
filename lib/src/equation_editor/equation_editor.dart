// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

import 'dart:async';

import 'package:eqnb/data.dart';
import 'package:eqlib/utils.dart';

import 'package:angular2/angular2.dart';
import 'package:angular2_components/angular2_components.dart';

import '../latex/latex.dart';

@Component(
  selector: 'equation-editor',
  templateUrl: 'equation_editor.html',
  styleUrls: const ['equation_editor.css'],
  directives: const [materialDirectives, LatexComponent],
  providers: const [materialProviders],
)
class EquationEditorComponent implements OnInit {
  final _onUpdate = new StreamController<EqInfo>();

  @Input()
  W<String> data;

  @Input()
  String latex;

  int tabindex = 0;

  bool editable = true;

  bool formatException = false;

  @ViewChild('input')
  MaterialInputComponent input;

  @Output()
  Stream<EqInfo> get onUpdate => _onUpdate.stream;

  void ngOnInit() {
    update();
    if (data.v.isNotEmpty) {
      hide();
    }
  }

  void update() {
    if (data.v.isEmpty) {
      return;
    }

    final eq = evaluateEquation(data.v);
    formatException = eq == null;
    if (!formatException) {
      _onUpdate.add(eq);
      editable = false;
    }
  }

  void edit() {
    editable = true;
    new Future.delayed(new Duration(milliseconds: 100), () {
      input.focus();
      tabindex = -1;
    });
  }

  void hide() {
    if (!formatException) {
      editable = false;
      tabindex = 0;
    }
  }
}
