// Copyright (c) 2016, Herman Bergwerf. All rights reserved.
// Use of this source code is governed by an AGPL-3.0-style license
// that can be found in the LICENSE file.

import 'package:angular2/platform/browser.dart';
import 'package:eqnb/eqnb.dart';

void main() {
  bootstrap(NotebookComponent);
}
